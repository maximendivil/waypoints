# README

Beetrack remote technical evaluation - Waypoints

### Ruby version
2.7.0

### Rails version
6.0.3.2

### System dependencies
- Ruby
- Rails
- PostgreSQL
- Redis

For dependencies installations, I recommend follow the next guide: https://gorails.com/setup

### Instalation
Install all the project gems with: `bundle install`.
Finally, run `yarn install --check-files`

### Database creation
Update `config/database.yml` with yours credentials and then run `rails db:create db:migrate`

### How to run the application
`bundle exec rails server` (by default it runs on port 3000, http://localhost:3000/)

### How to run the test suite
`bundle exec rspec`

### Services (job queues, cache servers, search engines, etc.)
`bundle exec sidekiq`
