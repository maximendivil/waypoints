class Vehicle < ApplicationRecord
  has_many :waypoints

  validates :identifier, presence: true
  validates :identifier, uniqueness: true
end
