class Waypoint < ApplicationRecord
  belongs_to :vehicle

  validates :latitude, presence: true
  validates :latitude, numericality: true
  validates :longitude, presence: true
  validates :longitude, numericality: true
  validates :sent_at, presence: true

  scope :most_recent, lambda {
    includes(:vehicle).joins(
      <<-SQL
      INNER JOIN (
        SELECT vehicle_id, MAX(sent_at) as sent_at
        FROM waypoints
        GROUP BY vehicle_id
      ) AS latest_waypoints ON waypoints.vehicle_id = latest_waypoints.vehicle_id AND waypoints.sent_at = latest_waypoints.sent_at
      SQL
    )
  }
end
