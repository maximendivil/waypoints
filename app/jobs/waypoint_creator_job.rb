class WaypointCreatorJob < ApplicationJob
  queue_as :default

  def perform(waypoint_params)
    Waypoint.create!(
      latitude: waypoint_params[:latitude],
      longitude: waypoint_params[:longitude],
      sent_at: waypoint_params[:sent_at],
      vehicle: Vehicle.find_or_initialize_by(identifier: waypoint_params[:vehicle_identifier])
    )
  end
end
