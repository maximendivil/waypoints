module Api
  module V1
    class WaypointsController < ApplicationController
      skip_before_action :verify_authenticity_token, only: [:create]

      def index
        render json: Waypoint.most_recent, status: :ok
      end

      def create
        return head :bad_request unless valid?

        WaypointCreatorJob.perform_later(waypoint_params)
      end

      private

      def valid?
        %i[latitude longitude sent_at vehicle_identifier].all? { |key| params[key].present? }
      end

      def waypoint_params
        params.permit(:latitude, :longitude, :sent_at, :vehicle_identifier)
      end
    end
  end
end
