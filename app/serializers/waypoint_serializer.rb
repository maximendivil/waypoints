class WaypointSerializer < ActiveModel::Serializer
  attributes :position, :vehicle_identifier, :sent_at

  def position
    {
      lat: object.latitude,
      lng: object.longitude
    }
  end

  def vehicle_identifier
    object.vehicle.identifier
  end

  def sent_at
    object.sent_at.strftime('%Y-%m-%d %H:%M:%S')
  end
end
