require 'rails_helper'

RSpec.describe Vehicle, type: :model do
  describe 'when creating a vehicle' do
    let(:vehicle) { build(:vehicle) }

    it 'creates with all attributes' do
      expect(vehicle).to be_valid
    end
  end
end
