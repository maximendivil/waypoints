require 'rails_helper'

RSpec.describe Waypoint, type: :model do
  describe 'when creating a vehicle' do
    let(:waypoint) { build(:waypoint) }

    it 'creates with all attributes' do
      expect(waypoint).to be_valid
    end
  end
end
