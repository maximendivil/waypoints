require 'rails_helper'

RSpec.describe WaypointCreatorJob, type: :job do
  let(:waypoint_params) do
    {
      latitude: Faker::Address.latitude.truncate(2),
      longitude: Faker::Address.latitude.truncate(2),
      sent_at: Time.current,
      vehicle_identifier: Faker::Vehicle.license_plate
    }.with_indifferent_access
  end

  describe '#perform_later' do
    it 'creates a waypoint' do
      ActiveJob::Base.queue_adapter = :test
      expect {
        WaypointCreatorJob.perform_later(waypoint_params)
      }.to have_enqueued_job.at(:no_wait)
    end
  end

  describe '#perform' do
    subject do
      WaypointCreatorJob.new.perform(waypoint_params)
    end

    it 'creates a waypoint and vehicle record' do
      expect { subject }.to change { Waypoint.count }.by(1)
                        .and change { Vehicle.count }.by(1)

      waypoint = Waypoint.last
      expect(waypoint.latitude).to eq(waypoint_params[:latitude])
      expect(waypoint.longitude).to eq(waypoint_params[:longitude])
      expect(waypoint.sent_at.to_s).to eq(waypoint_params[:sent_at].to_s)
      expect(waypoint.vehicle.identifier).to eq(waypoint_params[:vehicle_identifier])
    end

    context 'when vehicle already exists' do
      let!(:vehicle) { create(:vehicle) }

      before do
        waypoint_params[:vehicle_identifier] = vehicle.identifier
      end

      it 'creates only a waypoint record' do
        expect { subject }.to change { Waypoint.count }.by(1)
                          .and change { Vehicle.count }.by(0)
        waypoint = Waypoint.last
        expect(waypoint.vehicle).to eq(vehicle)
      end
    end
  end
end
