FactoryBot.define do
  factory :waypoint do
    association :vehicle, factory: :vehicle
    latitude { Faker::Address.latitude.truncate(2) }
    longitude { Faker::Address.longitude.truncate(2) }
    sent_at { Time.current }
  end
end
