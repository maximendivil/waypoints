require 'rails_helper'

RSpec.describe Api::V1::WaypointsController, type: :controller do
  describe 'GET #index' do
    subject { get :index }

    let!(:waypoints) { create_list(:waypoint, 3) }

    it 'returns the most recent waypoints' do
      subject
      json_response = JSON.parse(response.body)
      expect(json_response.size).to eq(3)
      waypoints.each do |waypoint|
        expect(json_response).to include(JSON.parse(WaypointSerializer.new(waypoint).to_json))
      end
    end

    context 'when there is more than one waypoint per vehicle' do
      let(:vehicle) { waypoints.first.vehicle }
      let!(:oldest_waypoint) do
        create(:waypoint, vehicle: vehicle, sent_at: Time.current.yesterday)
      end

      it 'not includes the oldest waypoint' do
        subject
        json_response = JSON.parse(response.body)
        expect(json_response).not_to include(JSON.parse(WaypointSerializer.new(oldest_waypoint).to_json))
      end
    end
  end

  describe 'POST #create' do
    let(:waypoint_params) do
      {
        latitude: Faker::Address.latitude,
        longitude: Faker::Address.latitude,
        sent_at: Time.current,
        vehicle_identifier: Faker::Vehicle.license_plate
      }.with_indifferent_access
    end

    subject do
      post :create, params: waypoint_params
    end

    before do
      allow(WaypointCreatorJob).to receive(:perform_later)
    end

    context 'when request is valid' do
      it 'responds with a valid status code' do
        subject
        expect(response).to have_http_status(:no_content)
      end
    end

    context 'when request is invalid' do
      before do
        attribute = waypoint_params.keys[rand(0..3)]
        waypoint_params[attribute] = nil
      end

      it 'responds with a bad request status code' do
        subject
        expect(response).to have_http_status(:bad_request)
      end
    end
  end
end
