class CreateVehicles < ActiveRecord::Migration[6.0]
  def change
    create_table :vehicles do |t|
      t.string :identifier, null: false, index: true

      t.timestamps
    end
  end
end
