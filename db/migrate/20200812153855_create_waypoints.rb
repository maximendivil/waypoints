class CreateWaypoints < ActiveRecord::Migration[6.0]
  def change
    create_table :waypoints do |t|
      t.float :latitude, null: false
      t.float :longitude, null: false
      t.datetime :sent_at, null: false, index: true
      t.references :vehicle, null: false, foreign_key: true, index: true

      t.timestamps
    end
  end
end
