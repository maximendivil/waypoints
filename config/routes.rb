require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :waypoints, only: %i[index create]
    end
  end

  get 'show', to: 'home#show'
  root 'home#show'
end
